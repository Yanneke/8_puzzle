#include "priority_queue/priorityqueue.h"
#include "puzzle.h"

struct tree_node{
	struct tree_node* parent;
	struct tree_node** children;
	short int nbChildren; 
	Puzzle* content; 
};

typedef struct tree_node TreeNode;

typedef TreeNode Tree;


Tree* createTree(Puzzle* start, Tree* parent);
void expand(Tree* root);
void solve(Puzzle* p);
void testHeap(Puzzle* p);
void testRBTree(Puzzle* p);
