#include "puzzle.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

int isSolvable(Puzzle* p){
	int i, j, tmp, tmp2, sum = 0;
	for(i = 0; i < N * N; i++){
		tmp = p->content[i/N][i%N];
		if(tmp != EMPTY){
			for(j = i + 1; j < N*N; j++){
				tmp2 = p->content[j/N][j%N];
				if(tmp2 != EMPTY && tmp2<tmp){
					sum++;
				}
			}
		}
	}
	return sum % 2 == 0;
}


int isSolved(Puzzle* p) {
	int i;
	for (i = 0; i < 8; i++) {
		if (p->content[i / 3][i % 3] != i + 1) {
			return FALSE;
		}
	}
	return p->content[2][2] == EMPTY;
}

int getHeuristic(Puzzle* p) {
	int i, j, val, res = 0;
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			if (p->content[i][j] != EMPTY) {
				val = p->content[i][j] - 1;
				res += abs(val / 3 - i) + abs((val % 3) - j);
			}
		}
	}
	return res;
}


void copyAndReset(Puzzle** element, Puzzle* model, Puzzle** result, short int* nbElements) {
	result[*nbElements] = *element;
	(*nbElements)++;
	*element = (Puzzle*) malloc(sizeof(Puzzle));
	memcpy(*element, model, sizeof(Puzzle));
}


void computePossibilities(Puzzle* p, Puzzle** res, short int* nbElements) {
	Puzzle* copy = (Puzzle*) malloc(sizeof(Puzzle));
	memcpy(copy, p, sizeof(Puzzle));
	*nbElements = 0;

	if (moveDown(copy)) {
		copyAndReset(&copy, p, res, nbElements);
	}
	if (moveLeft(copy)) {
		copyAndReset(&copy, p, res, nbElements);
	}
	if (moveUp(copy)) {
		copyAndReset(&copy, p, res, nbElements);
	}
	if (moveRight(copy)) {
		res[*nbElements] = copy;
		(*nbElements)++;
	}
}

int moveUp(Puzzle* p) {
	if (p->empty_x <= 0) {
		return FALSE;
	}
	p->content[p->empty_x][p->empty_y] = p->content[p->empty_x - 1][p->empty_y];
	p->empty_x = p->empty_x - 1;
	p->content[p->empty_x][p->empty_y] = EMPTY;
	return TRUE;
}

int moveDown(Puzzle* p) {
	if (p->empty_x >= 2) {
		return FALSE;
	}
	p->content[p->empty_x][p->empty_y] = p->content[p->empty_x + 1][p->empty_y];
	p->empty_x = p->empty_x + 1;
	p->content[p->empty_x][p->empty_y] = EMPTY;
	return TRUE;
}

int moveLeft(Puzzle* p) {
	if (p->empty_y <= 0) {
		return FALSE;
	}
	p->content[p->empty_x][p->empty_y] = p->content[p->empty_x][p->empty_y - 1];
	p->empty_y = p->empty_y - 1;
	p->content[p->empty_x][p->empty_y] = EMPTY;
	return TRUE;
}

int moveRight(Puzzle* p) {
	if (p->empty_y >= 2) {
		return FALSE;
	}
	p->content[p->empty_x][p->empty_y] = p->content[p->empty_x][p->empty_y + 1];
	p->empty_y = p->empty_y + 1;
	p->content[p->empty_x][p->empty_y] = EMPTY;
	return TRUE;
}

void print(Puzzle* p) {
	int i, j;
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			printf("%d  ", p->content[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

int fillRandom(Puzzle* p) {
	srand(time(NULL));
	int numbers[] = { EMPTY, 1, 2, 3, 4, 5, 6, 7, 8 };
	int len = 9;
	int i, j, r, index, result;
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			index = rand() % len;
			r = numbers[index];
			if (r == EMPTY) {
				result = i * 3 + j;
			}
			p->content[i][j] = r;
			len = listRemoveIndex(numbers, len, index);
		}
	}
	return result;
}

int listRemoveIndex(int* list, int listSize, int index) {
	while (index < listSize - 1) {
		list[index] = list[index + 1];
		index++;
	}
	return listSize - 1;
}

int listRemove(int* list, int listSize, int elem) {
	int i;
	for (i = 0; i < listSize; i++) {
		if (list[i] == elem) {
			return listRemoveIndex(list, listSize, i);
		}
	}
	return listSize;
}

Puzzle* newPuzzle() {
	Puzzle* p = (Puzzle*) calloc(1, sizeof(Puzzle));
	int empty = fillRandom(p);
	p->empty_x = empty / 3;
	p->empty_y = empty % 3;
	return p;
}
