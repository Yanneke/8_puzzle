OBJ_DIR = ./object_files


main: main.o puzzle.o tree.o priorityqueue.o rb_tree.o
	if [ ! -d $(OBJ_DIR) ]; then mkdir $(OBJ_DIR); fi 
	mv *.o $(OBJ_DIR)/
	cc -o 8_puzzle ./object_files/*.o
	
main.o: main.c puzzle.h constants.h tree.h
	cc -c main.c

puzzle.o: puzzle.c puzzle.h
	cc -c puzzle.c

tree.o: tree.c tree.h puzzle.h
	cc -c tree.c

priorityqueue.o: ./priority_queue/priorityqueue.c ./priority_queue/priorityqueue.h
	cc -c ./priority_queue/priorityqueue.c
	
rb_tree.o: ./rb_tree/rb_tree.c ./rb_tree/rb_tree.h
	cc -c ./rb_tree/rb_tree.c
