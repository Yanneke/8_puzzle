#include "tree.h"
#include <stdio.h>

int main(int argc, char** argv){
	Puzzle* p = newPuzzle();
	print(p);
	if(isSolvable(p)){
		solve(p);
	}else{
		printf("Not solvable\n");
	}
	return 0;
}
