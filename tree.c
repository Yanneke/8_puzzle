#include "tree.h"
#include "rb_tree/rb_tree.h"

#include <stdlib.h>
#include <stdio.h>

void expand(Tree* root) {
	Puzzle** res = (Puzzle**) malloc(4 * sizeof(Puzzle*));
	computePossibilities(root->content, res, &(root->nbChildren));
	root->children = (Tree**) malloc(root->nbChildren * sizeof(Tree*));
	int i = root->nbChildren - 1;
	while (i >= 0) {
		root->children[i] = createTree(res[i], root);
		i--;
	}
}

Tree* createTree(Puzzle* start, Tree* parent) {
	Tree* res = (Tree*) malloc(sizeof(Tree));
	res->parent = parent;
	res->content = start;
	res->nbChildren = 0;
	return res;
}


void print2(void* key, void* tree) {
	print(((Tree*) tree)->content);
	printf("Heuristic = %d\n", (*(int*) key));
}

int rbTreeComparisonFunction(struct rb_tree *self, struct rb_node *a,
		struct rb_node *b) {
	Puzzle* p1 = (Puzzle*) a->value;
	Puzzle* p2 = (Puzzle*) b->value;
	int i, j;
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			if (p1->content[i][j] != p2->content[i][j]) {
				return p1->content[i][j] - p2->content[i][j];
			}
		}
	}
	return 0;
}

void solve(Puzzle* p) {
	/**
	 * Declaring the variables.
	 */
	Tree* searchTree;
	PriorityQueue* queue;
	struct rb_tree* rbTree;
	int i, count = 0;
	int* key;

	/**
	 * Assigning initial values.
	 */
	key = (int*) malloc(sizeof(int));
	(*key) = getHeuristic(p);
	queue = createPriorityQueue(NB_CATEGORIES);
	searchTree = createTree(p, NULL);
	rbTree = rb_tree_create(rbTreeComparisonFunction);
	add(queue, (void*) searchTree, *key);
	rb_tree_insert(rbTree, (void*) searchTree);

	/**
	 * Looping for the actual exploration.
	 */
	do {
		if(pop(queue, (void**) &searchTree, key)== FALSE){
			return;
		}
		expand(searchTree);
		for (i = 0; i < searchTree->nbChildren; i++) {
			if (rb_tree_find(rbTree, searchTree->children[i]->content) == NULL) {
				key = (int*) malloc(sizeof(int));
				(*key) = getHeuristic(searchTree->children[i]->content);
				add(queue, searchTree->children[i], *key);
				rb_tree_insert(rbTree, searchTree->children[i]->content);
			}
		}
	} while (isSolved(searchTree->content) != TRUE);
	print(searchTree->content);
}

void testRBTree(Puzzle* p) {
	Tree* searchTree;
	PriorityQueue* queue;
	struct rb_tree* rbTree;
	int i, count = 0;
	int* key;

	/**
	 * Assigning initial values.
	 */
	key = (int*) malloc(sizeof(int));
	(*key) = getHeuristic(p);
	queue = createPriorityQueue(NB_CATEGORIES);
	searchTree = createTree(p, NULL);
	rbTree = rb_tree_create(rbTreeComparisonFunction);
	add(queue, (void*) searchTree, *key);

	if (rb_tree_find(rbTree, searchTree) != NULL) {
		printf("Error, should ot be present in the tree\n");
	}
	rb_tree_insert(rbTree, (void*) searchTree);
	if (rb_tree_find(rbTree, searchTree) == NULL) {
		printf("Error, should be present in the tree\n");
	}
	rb_tree_insert(rbTree, (void*) searchTree);
	printf("%lu\n", rb_tree_size(rbTree));
}

void testHeap(Puzzle* p) {
	Tree* t;
	PriorityQueue* queue;
	int i, count = 0;
	int* key;

	t = createTree(p, NULL);
	key = (int*) malloc(sizeof(int));
	(*key) = getHeuristic(p);
	queue = createPriorityQueue(NB_CATEGORIES);
	add(queue, (void*) t, *key);
	printf("Popping data...\n");
	pop(queue, (void**) &t, key);
	printf("Expanding tree\n");
	expand(t);
	for (i = 0; i < t->nbChildren; i++) {
		printf("%d\n", i);
		key = (int*) malloc(sizeof(int));
		(*key) = getHeuristic(t->children[i]->content);
		add(queue, t->children[i], *key);
	}
	for (i = 0; i < t->nbChildren; i++) {
		print(t->children[i]->content);
	}
}
