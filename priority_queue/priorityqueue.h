#include "../constants.h"

#define NB_CATEGORIES 24


struct queue_node{
	void* value;
	int key;
	struct queue_node* previous;
	struct queue_node* next;
};

typedef struct queue_node QueueNode;


QueueNode* createQueueNode(void* value, int key);



struct priorityqueue {
	int first;
	int last;
	QueueNode** heads;
	int length;
};

typedef struct priorityqueue PriorityQueue;


PriorityQueue* createPriorityQueue(int nbCategories);
void add(PriorityQueue* q, void* t, int key);
int pop(PriorityQueue* queue, void** t, int* key);
void testQueue();