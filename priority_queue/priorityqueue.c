#include "../priority_queue/priorityqueue.h"

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "../constants.h"

QueueNode* createQueueNode(void* value, int key){
	QueueNode* res = calloc(1, sizeof(QueueNode));
	res->value = value;
	res->key = key;
	return res;
}


PriorityQueue* createPriorityQueue(int nbCategories){
	PriorityQueue* res = calloc(1, sizeof(PriorityQueue));
	res->first = INT_MAX;
	res->last = INT_MIN;
	res->length = 0;
	res->heads = (QueueNode**) calloc(nbCategories, sizeof(QueueNode));
	return res;
}



void add(PriorityQueue* q, void* t, int key){
	QueueNode* newNode = createQueueNode(t, key);
	newNode->previous = NULL;
	if(q->heads[key] == NULL){
		newNode->next = NULL;
		q->heads[key] = newNode;
	}else{
		newNode->next =  q->heads[key];
		newNode->next->previous = newNode;
		q->heads[key] = newNode;
	}
	if(key < q->first){
		q->first = key;
	}
	if(key > q->last){
		q->last = key;
	}
	q->length++;
}


int pop(PriorityQueue* queue, void** t, int* key){
	if(queue->length <= 0){
		printf("The queue is empty\n");
		return FALSE;
	}
	(*key) = queue->first;
	(*t) = queue->heads[*key]->value;
	queue->heads[*key] = queue->heads[*key]->next;
	queue->length--;
	/**
	 * Update queue->first and queue->last values.
	 */
	if(queue->length > 0){
		int i = queue->first;
		while(queue->heads[i] == NULL && i <= queue->last){
			i++;
		}
		queue->first = i;
	} else{
		queue->first = INT_MAX;
		queue->last = INT_MIN;
	}
	return TRUE;
}


void testQueue(){
	PriorityQueue* q = createPriorityQueue(NB_CATEGORIES);
	int i;
	int* j;
	for(i = 0; i < 10; i++){
		j = (int*) malloc(sizeof(int));
		*j = i;
		printf("Adding %d\n", *j);
		add(q, j, i%3);
	}
	
	while(q->length > 0){
		pop(q, (void**)&j, &i);
		printf("Value = %d, Key = %d\n\n", *j, i);
		printf("Size of the queue = %d\n", q->length);
	}
}
