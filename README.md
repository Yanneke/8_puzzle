# 8-puzzle project

## Data structures
- This project uses a Red-Black tree as a set to avoid looping through elements.
- A priority queue is used, with the manhattan distance heuristic as priority

## Usage
- Create an initial puzzle with ```newPuzzle()```
- Check that it is feasible with ```isSolvable(Puzzle* p)```
- Call ```solve(Puzzle* p)``` function to solve the puzzle.