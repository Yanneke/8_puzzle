#include "constants.h"

typedef struct{
	short int content[3][3];
	short int empty_y;
	short int empty_x;
} Puzzle;


typedef struct{
	Puzzle** parent;
	Puzzle* value;
} Solution;

int moveUp(Puzzle* p);
int moveDown(Puzzle* p);
int moveLeft(Puzzle* p);
int moveRight(Puzzle* p);
void print(Puzzle* p);
Puzzle* newPuzzle();
int listRemove(int* list, int listSize, int elem);
int listRemoveIndex(int* list, int listSize, int index);
int fillRandom(Puzzle* p);
int isSolved(Puzzle* p);
void computePossibilities(Puzzle* p, Puzzle** res, short int* nbElements);
int getHeuristic(Puzzle* p);
int heuristicComparison(void* p1, void* p2);
int isSolvable(Puzzle* p);
